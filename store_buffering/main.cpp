#include <iostream>
#include <thread>

// Prevent compiler reordering
inline void CompilerBarrier() {
  asm volatile("" ::: "memory");
}

// Prevent store buffering
// https://www.felixcloutier.com/x86/mfence
inline void MemoryBarrier() {
  asm volatile("mfence" ::: "memory");
}

void StoreBuffering(size_t i) {
  // Shared
  int x = 0;
  int y = 0;

  // Local
  int r1 = 0;  // t1
  int r2 = 0;  // t2

  std::thread t1([&]() {
    x = 1;
    CompilerBarrier();
    //MemoryBarrier();
    r1 = y;
  });

  std::thread t2([&]() {
    y = 1;
    CompilerBarrier();
    //MemoryBarrier();
    r2 = x;
  });

  t1.join();
  t2.join();

  if (r1 == 0 && r2 == 0) {
    std::cout << "Iteration #" << i << ": Broken CPU!" << std::endl;
    std::abort();
  }
}

int main() {
  for (size_t i = 0; ; ++i) {
    StoreBuffering(i);
    if (i % 10000 == 0) {
      std::cout << "Iterations made: " << i + 1 << std::endl;
    }
  }
  return 0;
}
