#include <iostream>

#include <ring-buffer/ring_buffer.hpp>

int main() {
  SPSCRingBuffer<int> rb;
  rb.Publish(52);
  return 0;
}